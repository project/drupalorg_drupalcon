<?php

namespace Drupal\dcon_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Returns responses for Drupalcon Core Module routes.
 */
class DconCoreController extends ControllerBase {

  /**
   * Processes a checkboxes form element.
   */
  public static function processCheckboxes(&$element, FormStateInterface $form_state, &$complete_form) {
    foreach ($element as $id => &$value) {
      if (is_array($value) and isset($value["#attributes"])) {
        $value["#attributes"]["autocomplete"] = 'off';
      }
    }

    return $element;
  }

}
