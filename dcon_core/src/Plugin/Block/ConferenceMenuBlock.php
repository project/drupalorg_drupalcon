<?php

namespace Drupal\dcon_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Provides a Block to display the conference menu.
 *
 * @Block(
 *   id = "conference_menu_block",
 *   admin_label = @Translation("Conference Menu"),
 *   category = @Translation("Drupalcon"),
 * )
 */
class ConferenceMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $role_options[] = '- None -';

    $role_options = array_merge($role_options, [
      'main' => 'Main',
      'footer' => 'Footer',
    ]);

    $form['menu_role_selector'] = [
      '#type' => 'select',
      '#title' => t('Select the menu role'),
      '#default_value' => $config['menu_role_selector'] ?? '',
      '#weight' => 5,
      '#options' => $role_options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['menu_role_selector'] = $values['menu_role_selector'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = NULL;
    $menu_role = NULL;

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();

      $menu_name = NULL;

      foreach ($menus as $menu) {
        $conference_id = $menu->getThirdPartySetting('dcon_core', 'conference_selector');

        if ((int) $current_path_conference->id() === (int) $conference_id) {
          $menu_role = $menu->getThirdPartySetting('dcon_core', 'menu_role_selector');
          if (!empty($menu_role) and $menu_role === $config["menu_role_selector"]) {
            $menu_name = $menu->id();
            break;
          }
        }
      }

      if (isset($menu_name)) {
        $menu_parameters = new MenuTreeParameters();

        $menu_tree_service = \Drupal::service('menu.link_tree');
        $tree              = $menu_tree_service->load($menu_name, $menu_parameters);

        $manipulators = [
          ['callable' => 'menu.default_tree_manipulators:checkNodeAccess'],
          ['callable' => 'menu.default_tree_manipulators:checkAccess'],
          ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ];
        $tree = $menu_tree_service->transform($tree, $manipulators);

        $build['conference_menu'] = $menu_tree_service->build($tree);
        $build['conference_menu']['#attributes']['class'] = 'role-' . strtolower($menu_role);
      }
    }

    return $build;
  }

  public function getCacheTags() {
    $tags = [];

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      $tags[] = 'node:' . $current_path_conference->id();
      $config = $this->getConfiguration();

      $menus = \Drupal::entityTypeManager()
        ->getStorage('menu')
        ->loadMultiple();

      foreach ($menus as $menu) {
        $conference_id = $menu->getThirdPartySetting('dcon_core', 'conference_selector');

        if ((int) $current_path_conference->id() === (int) $conference_id) {
          $menu_role = $menu->getThirdPartySetting('dcon_core', 'menu_role_selector');
          if (!empty($menu_role) and $menu_role === $config["menu_role_selector"]) {
            $tags[] = 'menu-role:' . $menu_role;
          }
        }
      }

      return Cache::mergeTags(parent::getCacheTags(), $tags);
    } else {
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
