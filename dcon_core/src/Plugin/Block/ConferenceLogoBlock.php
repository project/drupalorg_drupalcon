<?php

namespace Drupal\dcon_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a Block to display the conference logo.
 *
 * @Block(
 *   id = "conference_logo_block",
 *   admin_label = @Translation("Conference Logo"),
 *   category = @Translation("Drupalcon"),
 * )
 */
class ConferenceLogoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = NULL;

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      $entity_type = 'node';
      $view_mode = 'logo';

      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
      $node = $storage->load($current_path_conference->id());
      $build = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'block-conference-logo',
          ],
        ],
      ];

      $build['node'] = $view_builder->view($node, $view_mode);
    }

    return $build;
  }

  public function getCacheTags() {

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $current_path_conference->id()));
    } else {
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
