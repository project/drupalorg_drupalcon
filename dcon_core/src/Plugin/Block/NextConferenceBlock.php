<?php

namespace Drupal\dcon_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;

/**
 * Provides a Block to display the next conference cta.
 *
 * @Block(
 *   id = "next_conference_block",
 *   admin_label = @Translation("Next Conference"),
 *   category = @Translation("Drupalcon"),
 * )
 */
class NextConferenceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = NULL;

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      $entity_type = 'node';
      $view_mode = 'next_conference';

      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
      $node = $storage->load($current_path_conference->id());
      $next_event = $node->get('field_next_event')->entity;

      if ($next_event instanceof Node) {
        $build['label'] = [
          '#markup' => '<div class="label">' . t('Next event') . '</div>',
        ];

        $build['conference'] = $view_builder->view($next_event, $view_mode);
      }
    }

    return $build;
  }

  public function getCacheTags() {

    $current_path_conference = _dcon_core_get_current_path_conference();
    if (!empty($current_path_conference)) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $current_path_conference->id()));
    } else {
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
