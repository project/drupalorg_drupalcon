<?php

namespace Drupal\dcon_core\Commands;

use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Session\UserSession;
use GuzzleHttp\Client;

/**
 * A drupalorg_drupalcon drush commands file.
 */
class DconCoreDrushCommands extends DrushCommands {

  /**
   * Do kuoni imports via the GraphQL endpoint.
   *
   * @command drupalorg_drupalcon:sync-kuoni-graphql
   */
  public function syncKuoniGraphQL() {
    $event_id = 'e6495abd-d528-4896-b96c-950dae430e08';
    $conference_id = 3947;
    $custom_field_definition_ids = [
      'AF3A0F9A-7FF3-46AC-B08E-12DD150737D2',
      '4FA2015B-D655-85EA-CC9D-3A107186AA63',
    ];

    // Get data from kuoni.
    $headers = \Drupal::config('drupalorg_drupalcon_kuoni_graphql')->get();
    $client = new Client();
    try {
      $response = $client->request(
        'POST',
        'https://ea-proxy.kuoni-congress.info/api/drupal/agenda',
        [
          'headers' => $headers,
          'form_params' => [
            'eventId' => $event_id,
            'offset' => 0,
            // This should be more than enough for all sessions.
            'limit' => 500,
            'customFieldsLimit' => 2,
            'input' => [
              'definitionIds' => $custom_field_definition_ids,
            ],
          ]
        ]
      );
      $kuoni_agenda_response = json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (\Throwable $e) {
      $this->logger('drupalorg_drupalcon')->error('Failed to get data from Kuoni GraphQL endpoint. See Error: @error', [
        '@error' => $e->getMessage(),
      ]);
      return;
    }

    // Process the data and put it in an array that we can manipulate easily.
    $sessions = [];
    $speaker_details_list = [];
    if (!empty($kuoni_agenda_response['event']['agenda']['items'])) {
      foreach ($kuoni_agenda_response['event']['agenda']['items'] as $session) {
        if (!empty($session)) {
          $extra_session_info = $session['presentations'][0] ?? NULL;
          $session_name = (strlen($session['name']) > 255) ?
            substr($session['name'],0,250) . '...' :
            $session['name'];
          $session_data = [
            'external_id' => $session['id'],
            'name' => $session_name,
            'start_date' => $session['date'] . 'T' . $session['start'] . ':00',
            'end_date' => $session['date'] . 'T' . $session['end'] . ':00',
            'location' => $session['location']['name'] ?? '',
            'speakers' => [],
            'description' => '',
            'track_name' => NULL,
            'tags' => NULL,
          ];
          if (!is_null($extra_session_info)) {
            $session_data['track_name'] = $extra_session_info['theme']['name'] ?? NULL;
            $session_data['description'] = $extra_session_info['documents'][0]['text'] ?? $extra_session_info['documents'][1]['text'] ?? NULL;
            $session_data['slides_link'] = $extra_session_info['documents'][0]['url'] ?? $extra_session_info['documents'][1]['url'] ?? NULL;
            if (!empty($extra_session_info['authors'])) {
              foreach ($extra_session_info['authors'] as $author) {
                if ($author['isPresenting'] && !empty($author['contact'])) {
                  $speaker = [
                    'first_name' => $author['contact']['firstName'] ?? '',
                    'last_name' => $author['contact']['lastName'] ?? '',
                    'username' => $author['contact']['username'] ?? '',
                  ];
                  $speaker_key = $speaker['first_name'] . $speaker['last_name'];
                  $session_data['speakers'][$speaker_key] = $speaker;
                  $speaker_details_list[$speaker_key] = $speaker;
                }
              }
            }
            if (!empty($extra_session_info['tags'])) {
              $session_data['tags'] = [];
              foreach ($extra_session_info['tags'] as $tag) {
                $session_data['tags'][] = $tag['name'];
              }
            }
            // Add custom fields dynamically.
            if (!empty($extra_session_info['customFields'])) {
              foreach ($extra_session_info['customFields'] as $customField) {
                $session_data[$customField['name']] = $customField['value'];
              }
            }
          }
          // Body descriptions are set like: "*** Section ***" to differentiate the sections.
          if (!is_null($session_data['description'])) {
            $session_data['description'] = str_replace('*** Captivating Introduction ***', '', $session_data['description']);
            $session_data['description'] = preg_replace('#\*{3} (.*?) \*{3}#', '<b>$1</b>', $session_data['description']);

            // Process custom fields and append them to the body.
            if (!empty($session_data['experience_level'])) {
              $session_data['description'] .= '<p><b>' . t('Experience level') . '</b><br>' . $session_data['experience_level'] . '</p>';
            }
            if (!empty($session_data['sponsor_logo'])) {
              $session_data['description'] .= '<p><b>' . t('Session sponsored by') . '</b><br><img style="max-width: 150px;" src="' . $session_data['sponsor_logo'] . '" alt="' . t('Sponsor logo') . '" /></p>';
            }
            if (!empty($session_data['slides_link'])) {
              if (!str_contains($session_data['slides_link'], '://') && filter_var('https://' . $session_data['slides_link'], FILTER_VALIDATE_URL) !== FALSE) {
                $session_data['slides_link'] = 'https://' . $session_data['slides_link'];
              }
              $session_data['description'] .= '<p><b>' . t('Slide Deck Presentation Link') . '</b><br><a href="' . $session_data['slides_link'] . '" target="_blank">Slides</a></p>';
            }
          }

          $sessions[$session['id']] = $session_data;
        }
      }

      // At this point, the data is in a predictable format. Try to find the session and update it first, otherwise create a new one.
      $track_data = $this->getTerms('track');
      $tag_data = $this->getTerms('tags');

      // Check for sessions id's already on site.
      echo "Checking for times and session id's already on EDO\n";
      $types = ['session'];
      $local_session_ext_id_list = $this->getCurrentSessionsIdToExternalIdMap($conference_id, $types);
      echo "End checking for times and session id's already on EDO\n";

      // dd($sessions);
      foreach ($sessions as $external_id => $session_data) {
        $session_speakers_string = '';
        if (!empty($session_data['speakers'])) {
          $session_speakers = [];
          foreach ($session_data['speakers'] as $speaker) {
            $speaker_string = $speaker['first_name'] . ' ' . $speaker['last_name'];
            if (
              !empty($speaker['username']) &&
              // If we find more patterns that users filled up, add them to the array.
              !in_array(strtolower($speaker['username']), ['n/a'])
            ) {
              $link = Link::fromTextAndUrl(
                $speaker['username'],
                Url::fromUri('https://www.drupal.org/u/' . $speaker['username'], [
                  'attributes' => [
                    'class' => ['druplicon'],
                  ],
                ])
              )->toString();
              $speaker_string .= ' (' . $link . ')';
            }
            $session_speakers[] = $speaker_string;
          }
          $session_speakers_string = implode(', ', $session_speakers);
        }

        // Tracks: format and save if we haven't seen it before.
        $session_track_machine_name = NULL;
        $undesireables = $this->getUndesireables();
        if (!empty($session_data['track_name'])) {
          // Turn the name into a machine name.wfwassfasffsffs
          $prewash = str_replace([
            "'",
            '?',
            ',',
          ], '', $session_data['track_name']);
          $track_machine_name = str_replace($undesireables, '_', $prewash);
          $session_track_machine_name = strtolower($track_machine_name);
          if (!array_key_exists($session_track_machine_name, $track_data) && $session_track_machine_name != '') {
            $track_term = $this->createTerm('track', $session_data['track_name'], $session_track_machine_name);
            $saved_machine_name = $track_term->get('machine_name')->getValue()[0]["value"];
            $saved_tid = $track_term->get('tid')->getValue()[0]["value"];
            $track_data[$saved_machine_name] = [
              'id' => $saved_tid,
              'machine_name' => $saved_machine_name,
            ];
            echo 'New session track saved ' . $session_data['track_name'] . ' with TID: ' . $saved_tid . "\n";
          }
        }

        // Tag
        $session_tags = $session_data['tags'];
        $session_tag_tids = [];
        $undesireables = $this->getUndesireables();
        if (isset($session_tags)) {
          foreach ($session_tags as $tag) {
            // Turn the name into a machine name.
            $prewash = str_replace([
              "'",
              '?',
              ',',
              "\r\n",
            ], '', $tag);
            $tag_machine_name = str_replace($undesireables, '_', $prewash);
            $session_tag_machine_name = strtolower($tag_machine_name);

            $session_tag_tid = NULL;
            if (array_key_exists($session_tag_machine_name, $tag_data)) {
              $session_tag_tid = $tag_data[$session_tag_machine_name]['id'];
            }
            elseif ($session_tag_machine_name != '') {
              $tag_term = $this->createTerm('tags', $tag, $session_tag_machine_name);
              $saved_machine_name = $tag_term->get('machine_name')->getValue()[0]["value"];
              $saved_tid = $tag_term->get('tid')->getValue()[0]["value"];
              $session_tag_tid = $saved_tid;
              $tag_data[$saved_machine_name] = [
                'id' => $saved_tid,
                'machine_name' => $saved_machine_name,
              ];
              echo 'New session tag saved ' . $session_tag_machine_name . ' with TID: ' . $saved_tid . "\n";
            }

            if (!is_null($session_tag_tid)) {
              $session_tag_tids[$session_tag_tid] = $session_tag_tid;
            }
          }
        }

        // Save this as an admin account, so we can text format properly.
        $accountSwitcher = \Drupal::service('account_switcher');
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));

        // Check if this session already exists.
        if (array_search($external_id, $local_session_ext_id_list)) {
          echo 'Session found, updating: ' . $session_data['name'] . " \n";
          $node = Node::load(array_search($external_id, $local_session_ext_id_list));
          $session_status = 'updated';
        }
        else {
          echo 'Creating new session: ' . $session_data['name'] . " \n";
          $node = Node::create(['type' => 'session']);
          $session_status = 'created';
        }

        if ($node) {
          $node->set('title', $session_data['name']);
          if (!empty($session_data['description'])) {
            $node->set('body', $session_data['description']);
            $node->body->format = 'full_html';
          }
          $node->set('field_parent_conference', $conference_id);
          $node->set('field_session_external_id', $external_id);
          //Sessions list gets filtered earlier in code, set type as "session"
          $node->set('field_session_type', 15);
          $node->set('field_speakers', $session_speakers_string) ?? '';
          $node->field_speakers->format = 'full_html';
          $node->set('field_location', $session_data['location'] ?? '');
          if (!empty($session_tag_tids)) {
            $node->field_tags->setValue($session_tag_tids);
          }
          else {
            $node->field_tags->setValue(NULL);
          }
          if (!empty($session_track_machine_name)) {
            $node->field_track->setValue($track_data[$session_track_machine_name]['id']);
          }
          else {
            $node->field_track->setValue(NULL);
          }
          $node->field_when = [
            'value' => $session_data['start_date'],
            'end_value' => $session_data['end_date'],
          ];
          $node->set('status', 1);
          $node->set('uid', 1);
          $node->save();
          echo 'Session ' . $session_status . ': ' . $session_data['name'] . " \n";
        }

        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
      }

      // Unpublish anything we have published that is a session and didn't come to us in the current import.
      $flipped_local_ids = array_flip($local_session_ext_id_list);
      $list_to_unpublish = array_diff_key($flipped_local_ids, $sessions);
      foreach ($list_to_unpublish as $nid) {
        if ($node_to_unpublish = $this->unpublishNode($nid)) {
          echo "Node " . $node_to_unpublish->label() . " is unpublished\n";
        }
      }

      echo "End of import\n";
      $this->logger('drupalorg_drupalcon')->notice('End of successful import from Kuoni GraphQL endpoint.');
    }
    else {
      $this->logger('drupalorg_drupalcon')->error('Empty response from Kuoni GraphQL endpoint.');
    }
  }

  /**
   * Do sessionize imports.
   *
   * @command drupalorg_drupalcon:sync-sessionize
   */
  public function syncSessionize() {

    // Import Configuration.
    $api_id = '12upnw66'; //DCATL2025
    // Nid of conference on staging cluster.
    $conference_id = 4545;
    $unpublish_question_id = 83037;
    $import_tag_confirm = 1;
    $import_track_confirm = 1;
    // Nid of parent conference on staging cluster.
    $parent_conference = 4545;
    $sessionize_sessions = [];
    $sessionize_speakers = [];
    $sessionize_categories = [];
    $sessionize_rooms = [];
    $node = [];
    // Get data from sessionize.
    echo "Start data request to sessionize\r\n";

    $client = new Client();
    try {
      $response = $client->request('GET', 'https://sessionize.com/api/v2/' . $api_id . '/view/All');
      $sessionize_agenda_response = json_decode($response->getBody()
        ->getContents());
    } catch (\Throwable $e) {
      echo "Failed to connect to sessionize: " . $e->getMessage() . "\n";
    }
    echo "End of data request to sessionize\r\n";
    echo "Start data sorting\r\n";
    $sessionize_sessions = $sessionize_agenda_response->sessions;
    $sessionize_speakers = $sessionize_agenda_response->speakers;
    $sessionize_categories = $sessionize_agenda_response->categories;
    $sessionize_rooms = $sessionize_agenda_response->rooms;

    // Organize session id's into an array.
    foreach ($sessionize_sessions as $session => $id) {
      $session_ids[$sessionize_sessions[$session]->title] = $id->id;
    }
    // Organize session speaker id's into an array.
    foreach ($sessionize_speakers as $key => $speaker) {
      $speaker_id_list[$sessionize_speakers[$key]->id] = $speaker->fullName;
      $speaker_id_array[$speaker->id] = $speaker;
      if (count($speaker->questionAnswers) >= 1) {
        foreach ($speaker->questionAnswers as $question) {
          // Question id 32525 && 42524 && 59307 is drupal.org username.
          if (in_array($question->questionId, [32525, 42524, 59307, 82995, 83018]) && !empty($question->answerValue)) {
            $speaker_username_list[$sessionize_speakers[$key]->id] = $question->answerValue;
            break;
          }
        }
      }
    }
    // Organize session room id's into an array.
    foreach ($sessionize_rooms as $key => $room) {
      $room_list[$sessionize_rooms[$key]->id] = $room->name;
    }
    // Organize session categories and id's into an array.
    foreach ($sessionize_categories as $key => $category_info) {
      foreach ($category_info->items as $key2 => $category_options) {
        $categories[$category_info->title][$category_options->id] = $category_options->name;
      }
    }

    // What tracks do we have already.
    if ($import_track_confirm == 1) {
      echo "Start checking for tracks we already have on EDO\n";
      $track_data = $this->getTerms('track');
    }
    echo "End checking for Tracks already on EDO\n";

    // What tags do we have already.
    if ($import_tag_confirm == 1) {
      echo "Start checking for tags already on EDO\n";
      $tag_data = $this->getTerms('tags');
    }
    echo "End checking for tags already on EDO\n";

    // What session types do we have? Lets map them to their tids.
    $local_session_type_data = $this->getTerms('session_type');

    // Check for sessionize session id's already on site.
    echo "Checking for times and session id's already on EDO\n";
    $types = [
      'session',
      'schedule_item',
      'social_event',
    ];
    $local_session_ext_id_list = $this->getCurrentSessionsIdToExternalIdMap($parent_conference, $types);
    echo "End checking for times and session id's already on EDO\n";

    // Check for local conferences so we can put them into an array to
    // reference for importing.
    $local_conference_list = $this->getAllConferences();

    // Loop over session list.
    foreach ($sessionize_sessions as $sessionize_session) {
      $session_speakers_string = '';

      echo "Importing " . $sessionize_session->title . ". \n";

      // Check to see if session has tracks we have seen before.
      $sessionize_session->track = '';
      $sessionize_session->track_id = '';

      if ($sessionize_session->categoryItems != '' && isset($categories['Content focus area'])) {
        foreach ($sessionize_session->categoryItems as $sess_track_check) {
          if (array_key_exists($sess_track_check, $categories['Content focus area'])) {
            $sessionize_session->track = $categories['Content focus area'][$sess_track_check];
            $sessionize_session->track_id = $sess_track_check;
          }
        }
      }

      // Tracks: format and save if we haven't seen it before.
      if ($import_track_confirm == 1) {
        $undesireables = $this->getUndesireables();

        if (isset($sessionize_session->track)) {
          $sessionize_session_track_machine_name = '';
          // Turn the name into a machine name.
          $prewash = str_replace([
            "'",
            '?',
            ',',
          ], '', $sessionize_session->track);
          $track_machine_name = str_replace($undesireables, '_', $prewash);
          // Define lowercased machine name into a variable.
          $sessionize_session_track_machine_name = strtolower($track_machine_name);

          if (array_key_exists($sessionize_session_track_machine_name, $track_data)) {
            $sessionize_session_track_tid = $track_data[$sessionize_session_track_machine_name]['machine_name'];
          }

          if (!array_key_exists($sessionize_session_track_machine_name, $track_data) && $sessionize_session_track_machine_name != '') {
            $track_term = $this->createTerm('track', $sessionize_session->track, $sessionize_session_track_machine_name);
            $saved_machine_name = $track_term->get('machine_name')->getValue();
            $saved_tid = $track_term->get('tid')->getValue();
            $sessionize_session_track_tid = $saved_tid[0]["value"];
            $track_data[$saved_machine_name[0]["value"]] = [
              'id' => $saved_tid[0]["value"],
              'machine_name' => $saved_machine_name[0]["value"],
            ];
            $tracks_data = $track_data[$saved_machine_name[0]["value"]];
            echo 'New session track saved ' . $sessionize_session->track . ' with TID: ' . $tracks_data['id'] . ', machine name: ' . $tracks_data['machine_name'] .  "\n";
          }
        }
      }

      // Tags: format and save if we haven't seen it before.
      if ($import_tag_confirm == 1) {
        $session_tags = [];
        $session_tags_list = [];
        $undesireables = $this->getUndesireables();
        foreach ($sessionize_session->categoryItems as $tag_check) {
          if (!empty($categories['Program Area'][$tag_check])) {
            $session_tags[$tag_check] = $categories['Program Area'][$tag_check];
          }
        }

        if ($session_tags != NULL) {
          $tag = '';
          foreach ($session_tags as $tag) {
            $session_tag_machine_name = '';
            // Turn the name into a machine name.
            $prewash = str_replace(["'", '?', "/ "], '', $tag);
            $tag_machine_name = str_replace($undesireables, '_', $prewash);
            // Define lowercased machine name into a variable.
            $session_tag_machine_name = strtolower($tag_machine_name);

            // If machine name already exists, get the tag id.
            if (!empty($session_tag_machine_name)) {

              if (isset($tag_data[$session_tag_machine_name])) {
                $session_tag_tid = $tag_data[$session_tag_machine_name]['id'];
                $session_tags_list[$session_tag_tid] = $tag;
              }
              // If the machine name isn't in our taxonomy already, save it and
              // get the term id.
              if (!array_key_exists($session_tag_machine_name, $tag_data)) {
                $term = $this->createTerm('tags', $tag, $session_tag_machine_name);
                $session_tag_tid = $term->tid;
                $saved_machine_name = $term->get('machine_name')->getValue();
                $saved_tid = $term->get('tid')->getValue();
                $tag_data[$saved_machine_name[0]["value"]] = [
                  'machine_name' => $saved_machine_name[0]["value"],
                  'id' => $saved_tid[0]['value'],
                ];
                $session_tags_list[$saved_tid[0]["value"]] = $tag;
                echo 'New session tag saved ' . $tag . ' with TID: ' . $tag_data[$saved_machine_name[0]["value"]] . "\n";
              }
            }
          }
        }
        foreach ($session_tags_list as $key => $tag_name) {
          $session_tag_tids[$key] = $key;
        }
      }

      // Time.
      $timezone = $local_conference_list[$conference_id]->field_time_zone->getValue('timezone')[0]['value'];
      date_default_timezone_set($timezone);
      $sessionize_session_time_start_unix = strtotime($sessionize_session->startsAt);
      $sessionize_session_time_stop_unix = strtotime($sessionize_session->endsAt);
      $sessionize_session_time_unix = $sessionize_session_time_start_unix . '-' . $sessionize_session_time_stop_unix;

      // Make sure session has a start time.
      if ($sessionize_session->startsAt == NULL) {
        continue;
      }
      unset($sessionize_speaker_name_links);
      // Speakers.
      $speaker = '';
      $sessionize_speaker_name_link_formatted = '';
      $session_speakers_string = '';
      $speakers_field_string_array = [];
      $session_speaker_name_links = array();
      // Iterate through session speakers if there are speakers
      if (count($sessionize_session->speakers) > 0) {
        foreach ($sessionize_session->speakers as $speaker) {
          if ($speaker == '17abb0f5-801e-4f18-ba87-0991606f6b9c' || $speaker == 'fa9f21c2-d70e-4d89-b601-0c140c8fbc21'){
            continue;
          }

          // Sessionize speaker profile link formatting
          if (count($speaker_id_array[$speaker]->links) > 0) {
            foreach ($speaker_id_array[$speaker]->links as $link) {
              if ($link->linkType == "Sessionize" && !empty($link->url)) {
                $sessionize_speaker_name_link_formatted = Link::FromTextAndUrl($speaker_id_array[$speaker]->firstName . ' ' . $speaker_id_array[$speaker]->lastName, Url::fromUri(($link->url)))
                  ->toString();
                $sessionize_speaker_name_link = $sessionize_speaker_name_link_formatted->getGeneratedLink();
                $sessionize_speaker_name_links[$speaker] = $sessionize_speaker_name_link;
                continue;
              }
            }
          }
          if (!empty($sessionize_speaker_name_links[$speaker]) && empty($speakers_field_string_array[$speaker])) {
            $speakers_field_string_array[$speaker] = $sessionize_speaker_name_links[$speaker];
          }

          // If the drupalorg speaker username list has a key that matches the speaker id in sessionize, format their username to link to their d.o profile and append it to their entry in the speaker array

          unset($drupalorg_user_link);
          if (!empty($speaker_username_list[$speaker])) {
            // Clean up user names coming in from sessionize.
            $drupalorg_user_name_trimmed = trim($speaker_username_list[$speaker]);
            $drupalorg_user_name_for_url = str_replace(' ', '-', $drupalorg_user_name_trimmed);

            // Format user name link.
            $options = [
              'attributes' => [
                'class' => ['druplicon'],
              ],
            ];

            if (!empty($drupalorg_user_name_for_url)) {
              $drupalorg_user_link = Link::fromTextAndUrl($drupalorg_user_name_trimmed, Url::fromUri('https://www.drupal.org/u/' . $drupalorg_user_name_for_url, $options))
                ->toString();
            }
            if (!empty($drupalorg_user_link)) {
              if (isset($speakers_field_string_array[$speaker])) {
                $speakers_field_string_array[$speaker] .= ' (' . $drupalorg_user_link . ')';
              }
              else {
                $speakers_field_string_array[$speaker] = $drupalorg_user_link;
              }
            }
            continue;
          }

          //If they don't have either just pass their first + last name string into the speaker array (rare).
          if (empty($drupalorg_user_link) && empty($sessionize_speaker_name_links)) {
            $speakers_field_string_array[$speaker] = $speaker_id_list[$speaker];
          }
        } //End of speaker loop

        // Alpha sort speaker names by first name because then we don't have
        // split names into first/last.
        //asort($speakers_field_string_array, SORT_STRING);

        // Remove Drupal Association from speaking at most sessions
        unset($speakers_field_string_array['17abb0f5-801e-4f18-ba87-0991606f6b9c']);
        unset($speakers_field_string_array['fa9f21c2-d70e-4d89-b601-0c140c8fbc21']);

        if (count($speakers_field_string_array) > 1) {
          $session_speakers_string = implode(', ', $speakers_field_string_array,);
        }
        else {
          $session_speakers_string = implode('', $speakers_field_string_array);
        }
      }

      $unpublish_session = 'false';
      // Body content.
      $body = '';
      $body = $sessionize_session->description;
      foreach ($sessionize_session->questionAnswers as $questionAnswer) {
        if ($questionAnswer->questionId == 42502 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Learning Objectives</h2>' . $questionAnswer->answerValue;
        }
        if ($questionAnswer->questionId == 42503 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Prerequisites</h2>' . $questionAnswer->answerValue;
        }
        if ($questionAnswer->questionId == 42518 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Synopsis</h2>' . $questionAnswer->answerValue;
        }
        //Unpublish check while we are looping through session questions
        if ($questionAnswer->questionId == $unpublish_question_id) {
          $unpublish_session = $questionAnswer->answerValue;
        }
      }
      // Session type check.
      if ($sessionize_session->isServiceSession == TRUE) {
        $session_type = 'schedule_item';
      }
      else {
        $session_type = 'session';
      }

      // Check if this session already exists.
      if (array_search($sessionize_session->id, $local_session_ext_id_list) != FALSE) {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as an admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // If it does, save existing session with updated data.
        // Create session object with data.
        echo 'Session found, updating: ' . $sessionize_session->title . " \n";
        $node = Node::load(array_search($sessionize_session->id, $local_session_ext_id_list));

        $node->title = $sessionize_session->title;

        $node->body = $body;
        $node->body->format = 'full_html';
        $roomId = $sessionize_session->roomId;
        $room_name = $room_list[$roomId];
        if (!empty($room_name)) {
          $node->field_location = $room_name;
        }
        $node->field_parent_conference = $parent_conference;
        $node->field_session_external_id = $sessionize_session->id;
        $node->field_session_type = $local_session_type_data[$session_type]['id'];
        if (!empty($session_speakers_string)) {
          $node->set('field_speakers', $session_speakers_string);
          $node->field_speakers->format = 'full_html';
        }
        if (!empty($session_tags_list)) {
          $node->field_tags = $session_tag_tids;
        }
        if (!empty($sessionize_session->track_id)) {
          $node->field_track = $track_data[$sessionize_session_track_machine_name]['id'];
        }
        $node->field_when = [
          'value' => $sessionize_session->startsAt,
          'end_value' => $sessionize_session->endsAt,
        ];
        $session_status = 'updated';
        if ($unpublish_session == 'true') {
          $node->set('status', 0);
          $session_status = 'unpublished';
        }
        else {
          $node->set('status', 1);
        }
        $node->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'Session ' . $session_status . ': ' . $sessionize_session->title . " \n";
      }
      else {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as the admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // External ID does not exist. Create new session object with data.
        echo 'Creating new session: ' . $sessionize_session->title . " \n";
        $new_session = Node::create(['type' => 'session']);
        $new_session->set('title', $sessionize_session->title);
        if (!empty($body)) {
          $new_session->set('body', $body);
          $new_session->body->format = 'full_html';
        }
        $new_session->set('field_location', $room_list[$sessionize_session->roomId]);
        $new_session->set('field_parent_conference', $parent_conference);
        $external_id = $sessionize_session->id;
        $new_session->set('field_session_external_id', $external_id);
        $new_session->set('field_session_type', $local_session_type_data[$session_type]['id']);
        if (!empty($session_speakers_string)) {
          $new_session->set('field_speakers', $session_speakers_string);
          $new_session->field_speakers->format = 'full_html';
        }
        $roomId = $sessionize_session->roomId;
        $room_name = $room_list[$roomId];
        if (!empty($room_name)) {
          $new_session->set('field_location', $room_name);
        }
        if (!empty($session_tags_list)) {
          $new_session->set('field_tags', $session_tag_tids);
        }
        if (!empty($sessionize_session->track_id)) {
          $new_session->set('field_track', $track_data[$sessionize_session_track_machine_name]['id']);
        }
        $new_session->field_when = [
          'value' => $sessionize_session->startsAt,
          'end_value' => $sessionize_session->endsAt,
        ];

        if ($unpublish_session == 'true') {
          $new_session->set('status', 0);
          echo "Session is unpublished: " . $sessionize_session->title . '.';
        }
        else {
          $new_session->set('status', 1);
        }
        $new_session->set('uid', 1);
        $new_session->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'New session saved: ' . $sessionize_session->title . " \n";
      }
      echo "End of session\n";
    }
    echo "End of import\n";
    $this->logger('drupalorg_drupalcon')->notice('End of successful import from Sessionize.');
  }


  /**
   * Do sessionize imports again.
   *
   * @command drupalorg_drupalcon:sync-sessionize-singapore
   */
  public function syncSessionizeSingapore() {

    // Import Configuration.
    $api_id = 'nak24sjx';
    // Nid of conference on staging cluster.
    $conference_id = 4292;
    $unpublish_question_id = 59326;
    $import_tag_confirm = 1;
    $import_track_confirm = 1;
    // Nid of parent conference on staging cluster.
    $parent_conference = 4292;
    $sessionize_sessions = [];
    $sessionize_speakers = [];
    $sessionize_categories = [];
    $sessionize_rooms = [];
    $node = [];
    //Question 79146 on a session is a binary where true means the session is about starshot
    // Get data from sessionize.
    echo "Start data request to sessionize\r\n";

    $client = new Client();
    try {
      $response = $client->request('GET', 'https://sessionize.com/api/v2/' . $api_id . '/view/All');
      $sessionize_agenda_response = json_decode($response->getBody()
        ->getContents());
    } catch (\Throwable $e) {
      echo "Failed to connect to sessionize: " . $e->getCode() . "\n";
      return 1;
    }
    echo "End of data request to sessionize\r\n";
    echo "Start data sorting\r\n";
    $sessionize_sessions = $sessionize_agenda_response->sessions;
    $sessionize_speakers = $sessionize_agenda_response->speakers;
    $sessionize_categories = $sessionize_agenda_response->categories;
    $sessionize_rooms = $sessionize_agenda_response->rooms;

    // Organize session id's into an array.
    foreach ($sessionize_sessions as $session => $id) {
      $session_ids[$sessionize_sessions[$session]->title] = $id->id;
    }
    // Organize session speaker id's into an array.
    foreach ($sessionize_speakers as $key => $speaker) {
      $speaker_id_list[$sessionize_speakers[$key]->id] = $speaker->fullName;
      $speaker_id_array[$speaker->id] = $speaker;
      if (count($speaker->questionAnswers) >= 1) {
        foreach ($speaker->questionAnswers as $question) {
          // Question id 32525 && 42524 && 59307 && 77534 is drupal.org username.
          if (in_array($question->questionId, [32525, 42524, 59307, 77534]) && !empty($question->answerValue)) {
            $speaker_username_list[$sessionize_speakers[$key]->id] = $question->answerValue;
            break;
          }
        }
      }
    }
    // Organize session room id's into an array.
    foreach ($sessionize_rooms as $key => $room) {
      $room_list[$sessionize_rooms[$key]->id] = $room->name;
    }
    // Organize session categories and id's into an array.
    foreach ($sessionize_categories as $key => $category_info) {
      foreach ($category_info->items as $key2 => $category_options) {
        $categories[$category_info->title][$category_options->id] = $category_options->name;
      }
    }

    // What tracks do we have already.
    if ($import_track_confirm == 1) {
      echo "Start checking for tracks we already have on EDO\n";
      $track_data = $this->getTerms('track');
    }
    echo "End checking for Tracks already on EDO\n";

    // What tags do we have already.
    if ($import_tag_confirm == 1) {
      echo "Start checking for tags already on EDO\n";
      $tag_data = $this->getTerms('tags');
    }
    echo "End checking for tags already on EDO\n";

    // What session types do we have? Lets map them to their tids.
    $local_session_type_data = $this->getTerms('session_type');

    // Check for sessionize session id's already on site.
    echo "Checking for times and session id's already on EDO\n";
    $types = [
      'session',
      'schedule_item',
      'social_event',
    ];
    $local_session_ext_id_list = $this->getCurrentSessionsIdToExternalIdMap($parent_conference, $types);
    echo "End checking for times and session id's already on EDO\n";

    // Check for local conferences so we can put them into an array to
    // reference for importing.
    $local_conference_list = $this->getAllConferences();

    // Loop over session list.
    foreach ($sessionize_sessions as $sessionize_session) {
      $session_speakers_string = '';

      echo "Importing " . $sessionize_session->title . ". \n";

      // Check to see if session has tracks we have seen before.
      $sessionize_session->track = '';
      $sessionize_session->track_id = '';

      if ($sessionize_session->categoryItems != '' && isset($categories['Session Track'])) {
        foreach ($sessionize_session->categoryItems as $sess_track_check) {
          if (array_key_exists($sess_track_check, $categories['Session Track'])) {
            $sessionize_session->track = $categories['Session Track'][$sess_track_check];
            $sessionize_session->track_id = $sess_track_check;
          }
        }
      }

      // Tracks: format and save if we haven't seen it before.
      if ($import_track_confirm == 1) {
        $undesireables = $this->getUndesireables();

        if (isset($sessionize_session->track)) {
          $sessionize_session_track_machine_name = '';
          // Turn the name into a machine name.
          $prewash = str_replace([
            "'",
            '?',
            ',',
          ], '', $sessionize_session->track);
          $track_machine_name = str_replace($undesireables, '_', $prewash);
          // Define lowercased machine name into a variable.
          $sessionize_session_track_machine_name = strtolower($track_machine_name);

          if (array_key_exists($sessionize_session_track_machine_name, $track_data)) {
            $sessionize_session_track_tid = $track_data[$sessionize_session_track_machine_name]['machine_name'];
          }

          if (!array_key_exists($sessionize_session_track_machine_name, $track_data) && $sessionize_session_track_machine_name != '') {
            $track_term = $this->createTerm('track', $sessionize_session->track, $sessionize_session_track_machine_name);
            $saved_machine_name = $track_term->get('machine_name')->getValue();
            $saved_tid = $track_term->get('tid')->getValue();
            $sessionize_session_track_tid = $saved_tid[0]["value"];
            $track_data[$saved_machine_name[0]["value"]] = [
              'id' => $saved_tid[0]["value"],
              'machine_name' => $saved_machine_name[0]["value"],
            ];
            echo 'New session track saved ' . $sessionize_session->track . ' with TID: ' . $track_data[$saved_machine_name[0]["value"]] . "\n";
          }
        }
      }

      // Tags: format and save if we haven't seen it before.
      if ($import_tag_confirm == 1) {
        $session_tags = [];
        $session_tags_list = [];
        $undesireables = $this->getUndesireables();
        foreach ($sessionize_session->categoryItems as $tag_check) {
          if (!empty($categories['Program Area'][$tag_check])) {
            $session_tags[$tag_check] = $categories['Program Area'][$tag_check];
          }
        }

        if ($session_tags != NULL) {
          $tag = '';
          foreach ($session_tags as $tag) {
            $session_tag_machine_name = '';
            // Turn the name into a machine name.
            $prewash = str_replace(["'", '?', "/ "], '', $tag);
            $tag_machine_name = str_replace($undesireables, '_', $prewash);
            // Define lowercased machine name into a variable.
            $session_tag_machine_name = strtolower($tag_machine_name);

            // If machine name already exists, get the tag id.
            if (!empty($session_tag_machine_name)) {

              if (isset($tag_data[$session_tag_machine_name])) {
                $session_tag_tid = $tag_data[$session_tag_machine_name]['id'];
                $session_tags_list[$session_tag_tid] = $tag;
              }
              // If the machine name isn't in our taxonomy already, save it and
              // get the term id.
              if (!array_key_exists($session_tag_machine_name, $tag_data)) {
                $term = $this->createTerm('tags', $tag, $session_tag_machine_name);
                $session_tag_tid = $term->tid;
                $saved_machine_name = $term->get('machine_name')->getValue();
                $saved_tid = $term->get('tid')->getValue();
                $tag_data[$saved_machine_name[0]["value"]] = [
                  'machine_name' => $saved_machine_name[0]["value"],
                  'id' => $saved_tid[0]['value'],
                ];
                $session_tags_list[$saved_tid[0]["value"]] = $tag;
                echo 'New session tag saved ' . $tag . ' with TID: ' . $tag_data[$saved_machine_name[0]["value"]] . "\n";
              }
            }
          }
        }
        foreach ($session_tags_list as $key => $tag_name) {
          $session_tag_tids[$key] = $key;
        }
      }

      // Time.
      $timezone = $local_conference_list[$conference_id]->field_time_zone->getValue('timezone')[0]['value'];
      date_default_timezone_set($timezone);
      $sessionize_session_time_start_unix = strtotime($sessionize_session->startsAt);
      $sessionize_session_time_stop_unix = strtotime($sessionize_session->endsAt);
      $sessionize_session_time_unix = $sessionize_session_time_start_unix . '-' . $sessionize_session_time_stop_unix;

      // Make sure session has a start time.
      if ($sessionize_session->startsAt == NULL) {
        continue;
      }
      unset($sessionize_speaker_name_links);
      // Speakers.
      $speaker = '';
      $sessionize_speaker_name_link_formatted = '';
      $session_speakers_string = '';
      $speakers_field_string_array = [];
      $session_speaker_name_links = array();
      // Iterate through session speakers if there are speakers
      if (count($sessionize_session->speakers) > 0) {
        foreach ($sessionize_session->speakers as $speaker) {
          if ($speaker == '17abb0f5-801e-4f18-ba87-0991606f6b9c' || $speaker == 'fa9f21c2-d70e-4d89-b601-0c140c8fbc21'){
            continue;
          }

          // Sessionize speaker profile link formatting
          if (count($speaker_id_array[$speaker]->links) > 0) {
            foreach ($speaker_id_array[$speaker]->links as $link) {
              if ($link->linkType == "Sessionize" && !empty($link->url)) {
                $sessionize_speaker_name_link_formatted = Link::FromTextAndUrl($speaker_id_array[$speaker]->firstName . ' ' . $speaker_id_array[$speaker]->lastName, Url::fromUri(($link->url)))
                  ->toString();
                $sessionize_speaker_name_link = $sessionize_speaker_name_link_formatted->getGeneratedLink();
                $sessionize_speaker_name_links[$speaker] = $sessionize_speaker_name_link;
                continue;
              }
            }
          }
          if (!empty($sessionize_speaker_name_links[$speaker]) && empty($speakers_field_string_array[$speaker])) {
            $speakers_field_string_array[$speaker] = $sessionize_speaker_name_links[$speaker];
          }

          // If the drupalorg speaker username list has a key that matches the speaker id in sessionize, format their username to link to their d.o profile and append it to their entry in the speaker array

          unset($drupalorg_user_link);
          if (!empty($speaker_username_list[$speaker])) {
            // Clean up user names coming in from sessionize.
            $drupalorg_user_name_trimmed = trim($speaker_username_list[$speaker]);
            $drupalorg_user_name_for_url = str_replace(' ', '-', $drupalorg_user_name_trimmed);

            // Format user name link.
            $options = [
              'attributes' => [
                'class' => ['druplicon'],
              ],
            ];

            if (!empty($drupalorg_user_name_for_url)) {
              $drupalorg_user_link = Link::fromTextAndUrl($drupalorg_user_name_trimmed, Url::fromUri('https://www.drupal.org/u/' . $drupalorg_user_name_for_url, $options))
                ->toString();
            }
            if (!empty($drupalorg_user_link)) {
              if (isset($speakers_field_string_array[$speaker])) {
                $speakers_field_string_array[$speaker] .= ' (' . $drupalorg_user_link . ')';
              }
              else {
                $speakers_field_string_array[$speaker] = $drupalorg_user_link;
              }
            }
            continue;
          }

          //If they don't have either just pass their first + last name string into the speaker array (rare).
          if (empty($drupalorg_user_link) && empty($sessionize_speaker_name_links)) {
            $speakers_field_string_array[$speaker] = $speaker_id_list[$speaker];
          }
        } //End of speaker loop

        // Alpha sort speaker names by first name because then we don't have
        // split names into first/last.
        //asort($speakers_field_string_array, SORT_STRING);

        // Remove Drupal Association from speaking at most sessions
        unset($speakers_field_string_array['17abb0f5-801e-4f18-ba87-0991606f6b9c']);
        unset($speakers_field_string_array['fa9f21c2-d70e-4d89-b601-0c140c8fbc21']);

        if (count($speakers_field_string_array) > 1) {
          $session_speakers_string = implode(', ', $speakers_field_string_array,);
        }
        else {
          $session_speakers_string = implode('', $speakers_field_string_array);
        }
      }

      $unpublish_session = 'false';
      // Body content.
      $body = '';
      $body = $sessionize_session->description;
      foreach ($sessionize_session->questionAnswers as $questionAnswer) {
        if ($questionAnswer->questionId == 42502 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Learning Objectives</h2>' . $questionAnswer->answerValue;
        }
        if ($questionAnswer->questionId == 42503 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Prerequisites</h2>' . $questionAnswer->answerValue;
        }
        if ($questionAnswer->questionId == 42518 && ($questionAnswer->answerValue != 'None' || '')) {
          $body .= '<br><h2>Synopsis</h2>' . $questionAnswer->answerValue;
        }
        //Unpublish check while we are looping through session questions
        if ($questionAnswer->questionId == $unpublish_question_id) {
          $unpublish_session = $questionAnswer->answerValue;
        }
      }
      // Session type check.
      if ($sessionize_session->isServiceSession == TRUE) {
        $session_type = 'schedule_item';
      }
      else {
        $session_type = 'session';
      }

      // Check if this session already exists.
      if (array_search($sessionize_session->id, $local_session_ext_id_list) != FALSE) {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as an admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // If it does, save existing session with updated data.
        // Create session object with data.
        echo 'Session found, updating: ' . $sessionize_session->title . " \n";
        $node = Node::load(array_search($sessionize_session->id, $local_session_ext_id_list));

        $node->title = $sessionize_session->title;

        $node->body = $body;
        $node->body->format = 'full_html';
        $roomId = $sessionize_session->roomId;
        $room_name = $room_list[$roomId];
        if (!empty($room_name)) {
          $node->field_location = $room_name;
        }
        $node->field_parent_conference = $parent_conference;
        $node->field_session_external_id = $sessionize_session->id;
        $node->field_session_type = $local_session_type_data[$session_type]['id'];
        if (!empty($session_speakers_string)) {
          $node->set('field_speakers', $session_speakers_string);
          $node->field_speakers->format = 'full_html';
        }
        if (!empty($session_tags_list)) {
          $node->field_tags = $session_tag_tids;
        }
        if (!empty($sessionize_session->track_id)) {
          $node->field_track = [$track_data[$sessionize_session_track_machine_name]['id']];
          foreach ($sessionize_session->questionAnswers as $questionAnswer) {
            // Check if it's a starshot session
            if (!empty($questionAnswer->questionId) && ($questionAnswer->questionId == 79146) && ($questionAnswer->answerValue == 'true')) {
              // If it is, add a drupal_starshot track
              $node->field_track = [$track_data[$sessionize_session_track_machine_name]['id'], $track_data['drupal_starshot']['id']];
            }
          }
        }
        $node->field_when = [
          'value' => $sessionize_session->startsAt,
          'end_value' => $sessionize_session->endsAt,
        ];
        $session_status = 'updated';
        if ($unpublish_session == 'true') {
          $node->set('status', 0);
          $session_status = 'unpublished';
        }
        else {
          $node->set('status', 1);
        }
        $node->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'Session ' . $session_status . ': ' . $sessionize_session->title . " \n";
      }
      else {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as the admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // External ID does not exist. Create new session object with data.
        echo 'Creating new session: ' . $sessionize_session->title . " \n";
        $new_session = Node::create(['type' => 'session']);
        $new_session->set('title', $sessionize_session->title);
        if (!empty($body)) {
          $new_session->set('body', $body);
          $new_session->body->format = 'full_html';
        }
        $new_session->set('field_location', $room_list[$sessionize_session->roomId]);
        $new_session->set('field_parent_conference', $parent_conference);
        $external_id = $sessionize_session->id;
        $new_session->set('field_session_external_id', $external_id);
        $new_session->set('field_session_type', $local_session_type_data[$session_type]['id']);
        if (!empty($session_speakers_string)) {
          $new_session->set('field_speakers', $session_speakers_string);
          $new_session->field_speakers->format = 'full_html';
        }
        $roomId = $sessionize_session->roomId;
        $room_name = $room_list[$roomId];
        if (!empty($room_name)) {
          $new_session->set('field_location', $room_name);
        }
        if (!empty($session_tags_list)) {
          $new_session->set('field_tags', $session_tag_tids);
        }
        if (!empty($sessionize_session->track_id)) {
          $new_session->set('field_track', $track_data[$sessionize_session_track_machine_name]['id']);
          foreach ($sessionize_session->questionAnswers as $questionAnswer) {
            // Check if it's a starshot session
            if (!empty($questionAnswer->questionId) && ($questionAnswer->questionId == 79146) && ($questionAnswer->answerValue == 'true')) {
              // If it is, add a drupal_starshot track
              $new_session->set('field_track', [$track_data[$sessionize_session_track_machine_name]['id'], $track_data['drupal_starshot']['id']]);
            }
          }
        }
        $new_session->field_when = [
          'value' => $sessionize_session->startsAt,
          'end_value' => $sessionize_session->endsAt,
        ];

        if ($unpublish_session == 'true') {
          $new_session->set('status', 0);
          echo "Session is unpublished: " . $sessionize_session->title . '.';
        }
        else {
          $new_session->set('status', 1);
        }
        $new_session->set('uid', 1);
        $new_session->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'New session saved: ' . $sessionize_session->title . " \n";
      }
      echo "End of session\n";
    }
    echo "End of import\n";
    $this->logger('drupalorg_drupalcon')->notice('End of successful import from Sessionize.');
  }

  /**
   * Do kuoni imports.
   *
   * @command drupalorg_drupalcon:sync-kuoni
   */
  public function syncKuoni() {

    // Nid of conference on staging cluster.
    $conference_id = 3114;
    $import_tag_confirm = 1;
    $import_track_confirm = 1;
    // Nid of parent conference on staging cluster.
    $parent_conference = 3114;
    $kuoni_sessions = [];
    $kuoni_speakers = [];
    $kuoni_rooms = [];
    $node = [];
    // See what we already have
    // What tracks do we have already.
    if ($import_track_confirm == 1) {
      echo "Start checking for tracks we already have on EDO\n";
      $track_data = $this->getTerms('track');
    }
    echo "End checking for Tracks already on EDO\n";

    // What tags do we have already.
    if ($import_tag_confirm == 1) {
      echo "Start checking for tags already on EDO\n";
      $tag_data = $this->getTerms('tags');
    }
    echo "End checking for tags already on EDO\n";

    // What session types do we have? Lets map them to their tids.
    $local_session_type_data = $this->getTerms('session_type');

    // Check for session id's already on site.
    echo "Checking for times and session id's already on EDO\n";
    $types = [
      'session',
      'schedule_item',
      'social_event',
      'ConcurrentSession',
    ];
    $local_session_ext_id_list = $this->getCurrentSessionsIdToExternalIdMap($parent_conference, $types);
    echo "End checking for types and session id's already on EDO\n";

    // Check for local conferences so we can put them into an array to
    // reference for importing.
    $local_conference_list = $this->getAllConferences();

    // Get data from /GetAgendas and/or /GetPresenters
    $headers = \Drupal::config('drupalorg_drupalcon_kuoni_2023_headers')->get();
    $client = new Client();
    try {
      $response = $client->request('GET', 'https://kuonicongress.eventsair.com/DoubleDutchAPI/drupalcon-lille-2023/api/Api/GetAgendas', ['headers' => $headers,]);
      $kuoni_agenda_response = json_decode($response->getBody()->getContents());
      $response = $client->request('GET', 'https://kuonicongress.eventsair.com/DoubleDutchAPI/drupalcon-lille-2023/api/Api/GetPresenters', ['headers' => $headers,]);
      $kuoni_speakers_response = json_decode($response->getBody()
        ->getContents());
    } catch (\Throwable $e) {
      $this->logger('drupalorg_drupalcon')
        ->error('Failed to get data from Kuoni. See Error: @error', [
          '@error' => $e->getMessage(),
        ]);
      return;
    }

    echo "End of data request to Kuoni\r\n";
    echo "Start data sorting\r\n";
    foreach ($kuoni_agenda_response->Agendas as $session) {
      // Filter nonsessions out of session list
      if (
        !in_array($session->Type, ['ConcurrentSession', 'Session']) ||
        str_contains($session->Name, 'No Presentation') ||
        str_contains($session->Name, 'BoF')) {
        continue;
      }
      $kuoni_sessions[$session->Id] = $session;
    }

    foreach ($kuoni_sessions as $session) {
      // Unset all the things that might still be set if data is missing from a session
      unset($session_speakers);
      unset($session_speakers_string);
      unset($body);
      unset($location);
      unset($session_tag_tids);

      if (!empty($session->SessionPresentationDetails->Presenters)) {
        // Time
        $start_string = $session->Date . 'T' . substr($session->StartTime, 0, 2) . ':' . substr($session->StartTime, 2, 2) . ':00';
        $end_string = $session->Date . 'T' . substr($session->EndTime, 0, 2) . ':' . substr($session->EndTime, 2, 2) . ':00';

        // Content
        if (!empty($session->SessionPresentationDetails->Presenters[0]->Documents[0]->TextContent)) {
          $body = $session->SessionPresentationDetails->Presenters[0]->Documents[0]->TextContent . '<br>' . $session->SessionPresentationDetails->Presenters[0]->PresentationType;

          if(!empty($session->SessionPresentationDetails->Presenters[0]->PaperReference)){
            $body .= '<br><h2>Experience level of the audience</h2><br>'.$session->SessionPresentationDetails->Presenters[0]->PaperReference;
          }
        }

        // Speakers
        $drupalorg_username = '';
        // Format user name link.
        $options = [
          'attributes' => [
            'class' => ['druplicon'],
          ],
        ];

        foreach ($kuoni_speakers_response->Presenters as $key => $presenter_details) {
          $speaker_details_list[$presenter_details->FirstName . $presenter_details->LastName] = $presenter_details;
          if ($presenter_details->InstagramUrl != '') {
            $drupalorg_username = substr($presenter_details->InstagramUrl, 8);
            $drupalorg_user_links[$presenter_details->FirstName . $presenter_details->LastName] = Link::fromTextAndUrl($drupalorg_username, Url::fromUri('https://www.drupal.org/u/' . $drupalorg_username, $options))
              ->toString();

            $speaker_details_list[$presenter_details->FirstName . $presenter_details->LastName]->drupalorg_link = $drupalorg_user_links[$presenter_details->FirstName . $presenter_details->LastName];
          }
        }

        $session_speakers = [];
        foreach ($session->SessionPresentationDetails->Presenters[0]->Authors as $key => $presenter) {
          if ($presenter->Presenting == TRUE) {
            $session_speakers[$presenter->FirstName . $presenter->LastName] = $presenter->FirstName . ' ' . $presenter->LastName;
          }
        }
        foreach ($session_speakers as $name => $data) {

          if (isset($drupalorg_user_links[$name])) {
            $session_speakers[$name] .= ' (' . $drupalorg_user_links[$name] . ')';
          }
        }

        if (is_array($session_speakers)) {
          $session_speakers_string = implode(', ', $session_speakers);
        }
      }
      if (empty($body)) {
        $body = '';
      }


      // Track
      if (!empty($session->SessionPresentationDetails->Presenters[0]->Theme)) {
        $session_track = strtolower($session->SessionPresentationDetails->Presenters[0]->Theme);
      }

      // Tracks: format and save if we haven't seen it before.
      if ($import_track_confirm == 1) {
        $undesireables = $this->getUndesireables();

        if (isset($session_track)) {
          $session_track_machine_name = '';
          // Turn the name into a machine name.wfwassfasffsffs
          $prewash = str_replace([
            "'",
            '?',
            ',',
          ], '', $session_track);
          $track_machine_name = str_replace($undesireables, '_', $prewash);
          // Define lowercased machine name into a variable.
          $session_track_machine_name = strtolower($track_machine_name);

          if (array_key_exists($session_track_machine_name, $track_data)) {
            $session_track_tid = $track_data[$session_track_machine_name]['id'];
          }

          if (!array_key_exists($session_track_machine_name, $track_data) && $session_track_machine_name != '') {
            $track_term = $this->createTerm('track', $session_track, $session_track_machine_name);
            $saved_machine_name = $track_term->get('machine_name')->getValue();
            $saved_tid = $track_term->get('tid')->getValue();
            $session_track_tid = $saved_tid[0]["value"];
            $track_data[$saved_machine_name[0]["value"]] = [
              'id' => $saved_tid[0]["value"],
              'machine_name' => $saved_machine_name[0]["value"],
            ];
            echo 'New session track saved ' . $session_track . ' with TID: ' . $track_data[$saved_machine_name[0]["value"]] . "\n";
          }
        }
      }

      // Tag
      if (!empty($session->Comment)) {
        $session_tags = strtolower($session->Comment);
      }
      // Need to explode PaperReference into an array of tags
      $session_tags = array_map('trim', explode(',', $session->Comment));

      // Tags: format and save if we haven't seen it before.
      if ($import_tag_confirm == 1) {
        $undesireables = $this->getUndesireables();

        if (isset($session_tags)) {
          foreach ($session_tags as $tag) {
            $session_tag_machine_name = '';
            // Turn the name into a machine name.
            $prewash = str_replace([
              "'",
              '?',
              ',',
              "\r\n",
            ], '', $tag);
            $tag_machine_name = str_replace($undesireables, '_', $prewash);
            // Define lowercased machine name into a variable.
            $session_tag_machine_name = strtolower($tag_machine_name);

            $session_tag_tid = NULL;
            if (array_key_exists($session_tag_machine_name, $tag_data)) {
              $session_tag_tid = $tag_data[$session_tag_machine_name]['id'];
            }

            if (!array_key_exists($session_tag_machine_name, $tag_data) && $session_tag_machine_name != '') {
              $tag_term = $this->createTerm('tags', $tag, $session_track_machine_name);
              $saved_machine_name = $tag_term->get('machine_name')->getValue();
              $saved_tid = $tag_term->get('tid')->getValue();
              $session_tag_tid = $saved_tid[0]["value"];
              $tag_data[$saved_machine_name[0]["value"]] = [
                'id' => $saved_tid[0]["value"],
                'machine_name' => $saved_machine_name[0]["value"],
              ];
              echo 'New session tag saved ' . $session_tag_machine_name . ' with TID: ' . $tag_data[$saved_machine_name[0]["value"]]['id'] . "\n";
            }

            if (!is_null($session_tag_tid)) {
              $session_tag_tids[$session_tag_tid] = $session_tag_tid;
            }
          }
        }
      }

      // Location
      $location = $session->Location;

      // External ID
      $session_external_id = $session->Id;

      // Check if this session already exists.
      if (array_search($session_external_id, $local_session_ext_id_list) != FALSE) {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as an admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // If it does, save existing session with updated data.
        // Update session object with data.
        echo 'Session found, updating: ' . $session->Name . " \n";
        $node = Node::load(array_search($session_external_id, $local_session_ext_id_list));
        $node->title = $session->Name;
        $node->body = $body;
        $node->body->format = 'full_html';
        if (!empty($location)) {
          $node->field_location = $location;
        }
        else {
          $node->field_location = '';
        }
        $node->field_parent_conference = $parent_conference;
        $node->field_session_external_id = $session->Id;
        $node->set('field_session_type', 15); //Sessions list gets filtered earlier in code, set type as "session"
        if (!empty($session_speakers_string)) {
          $node->set('field_speakers', $session_speakers_string);
          $node->field_speakers->format = 'full_html';
        }
        else {
          $node->set('field_speakers', '');
        }

        if (!empty($session_tag_tids)) {
          $node->field_tags->setValue($session_tag_tids);
        }
        else {
          $node->field_tags->setValue(NULL);
        }

        if (!empty($session_track_machine_name)) {
          $node->field_track->setValue($track_data[$session_track_machine_name]['id']);
        }
        else {
          $node->field_track->setValue(NULL);
        }

        $node->field_when = [
          'value' => $start_string,
          'end_value' => $end_string,
        ];

        $session_status = 'updated';

        $node->set('status', 1);
        $node->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'Session ' . $session_status . ': ' . $session->Name . " \n";
      }
      else {
        $accountSwitcher = \Drupal::service('account_switcher');
        // Save this as the admin account, so we can text format properly.
        $accountSwitcher->switchTo(new UserSession(['uid' => 1]));
        // External ID does not exist. Create new session object with data.
        echo 'Creating new session: ' . $session->Name . " \n";
        $new_session = Node::create(['type' => 'session']);
        $new_session->set('title', $session->Name);
        if (!empty($body)) {
          $new_session->set('body', $body);
          $new_session->body->format = 'full_html';
        }
        $new_session->set('field_location', $location);
        $new_session->set('field_parent_conference', $parent_conference);
        $external_id = $session_external_id;
        $new_session->set('field_session_external_id', $external_id);
        $new_session->set('field_session_type', 15); //Sessions list gets filtered earlier in code, set type as "session"
        if (!empty($session_speakers_string)) {
          $new_session->set('field_speakers', $session_speakers_string);
          $new_session->field_speakers->format = 'full_html';
        }
        if (!empty($room_name)) {
          $new_session->set('field_location', $location);
        }
        if (!empty($session_tags_list)) {
          $new_session->set('field_tags', $session_tag_tids);
        }
        if (!empty($session->track_id)) {
          $new_session->set('field_track', $track_data[$session_track_machine_name]['id']);
        }
        $new_session->field_when = [
          'value' => $start_string,
          'end_value' => $end_string,
        ];
        $new_session->set('status', 1);
        $new_session->set('uid', 1);
        $new_session->save();
        // Making sure we always switch back. Very important if you using u1.
        $accountSwitcher->switchBack();
        echo 'New session saved: ' . $session->Name . " \n";
      }

      echo "End of session\n";
    }


    // Unpublish anything we have published that is a session and didn't come to us in the current import.
    $flipped_local_ids = array_flip($local_session_ext_id_list);
    $list_to_unpublish = array_diff_key($flipped_local_ids, $kuoni_sessions);
    foreach ($list_to_unpublish as $nid) {
      if ($node_to_unpublish = $this->unpublishNode($nid)) {
        echo "Node " . $node_to_unpublish->label() . " is unpublished\n";
      }
    }

    echo "End of import\n";
    $this->logger('drupalorg_drupalcon')->notice('End of successful import from Kuoni.');
  }

  /**
   * Get terms of a vocabulary.
   *
   * @param $vid
   * @return array
   */
  protected function getTerms($vid = NULL) {
    $data = [];
    if ($vid) {
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vid);
      foreach ($terms as $term) {
        $data[$term->machine_name] = [
          'id' => $term->tid,
          'name' => $term->name,
          'machine_name' => $term->machine_name,
        ];
      };
    }

    return $data;
  }

  /**
   * @return string[]
   */
  protected function getUndesireables() {
    return [
      ' & ',
      ' ',
      "'",
      '-',
      '\'',
      "/",
      "/ ",
      "(",
      ")",
      ", ",
      ",_",
    ];
  }

  /**
   * Gets a map from session Drupal ID to external ID for a conference.
   * @param $parent_conference_id
   * @param $types
   * @return array
   */
  protected function getCurrentSessionsIdToExternalIdMap($parent_conference_id, $types = []) {
    if (empty($types)) {
      return [];
    }

    $local_sessions_nids = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', $types, 'IN')
      ->condition('field_parent_conference', $parent_conference_id)
      ->execute();
    $local_sessions = Node::loadMultiple($local_sessions_nids);
    $local_session_ext_id_list = [];
    foreach ($local_sessions as $local_session) {
      if ($local_session->hasField('field_session_external_id')) {
        $external_id = $local_session->get('field_session_external_id')->getValue()[0]['value'];
        $local_session_ext_id_list[$local_session->id()] = $external_id;
      }
    }

    return $local_session_ext_id_list;
  }

  protected function getAllConferences() {
    $conference_nids = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', 'conference')
      ->execute();
    return Node::loadMultiple($conference_nids);
  }

  protected function createTerm($vocabulary, $name, $machine_name) {
    $term = Term::create([
      'vid' => $vocabulary,
      'name' => strtolower($name),
      'machine_name' => $machine_name,
    ]);
    $term->save();

    return $term;
  }

  protected function unpublishNode($nid) {
    $node_to_unpublish = Node::load($nid);
    if (
      $node_to_unpublish &&
      $node_to_unpublish->bundle() === 'session'
    ) {
      if ($node_to_unpublish->isPublished()) {
        $node_to_unpublish->setUnpublished();
        $node_to_unpublish->save();
      }

      return $node_to_unpublish;
    }

    return FALSE;
  }

}
